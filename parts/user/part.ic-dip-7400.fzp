<?xml version="1.0" encoding="UTF-8"?>
<module fritzingVersion="0.9.3b" moduleId="03d01c42-e8aa-4255-8f2b-b2d0fd710675">
	<version>4</version>
	<author>HKGoldenMr.A</author>
	<title>7400</title>
	<description>7400 contains 4 independent 2-input NAND gate.</description>
	<date>2021-03-28</date>
	<label>IC</label>
	<tags>
		<tag>IC</tag>
		<tag>DIP</tag>
	</tags>
	<properties>
		<property name="family">A IC</property>
		<property name="package">DIP</property>
		<property name="pins">14</property>
		<property name="type">7400</property>
	</properties>
	<views>
		<iconView>
			<layers image="icon/ic-dip-7400.svg">
				<layer layerId="icon"/>
			</layers>
		</iconView>
		<breadboardView>
			<layers image="breadboard/ic-dip-7400.svg">
				<layer layerId="breadboard"/>
			</layers>
		</breadboardView>
		<schematicView>
			<layers image="schematic/ic-dip-7400.svg">
				<layer layerId="schematic"/>
			</layers>
		</schematicView>
		<pcbView>
			<layers image="pcb/ic-dip-7400.svg">
				<layer layerId="copper1"/>
				<layer layerId="silkscreen"/>
				<layer layerId="copper0"/>
			</layers>
		</pcbView>
	</views>
	<connectors>
		<connector id="connector-I0A" name="I0A" type="male">
			<description>Input 0A</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-I0A-pin" terminalId="connector-I0A-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-I0A-pin" terminalId="connector-I0A-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-I0A-pad"/>
					<p layer="copper0" svgId="connector-I0A-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-I0B" name="I0B" type="male">
			<description>Input 0B</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-I0B-pin" terminalId="connector-I0B-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-I0B-pin" terminalId="connector-I0B-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-I0B-pad"/>
					<p layer="copper0" svgId="connector-I0B-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-O0" name="O0" type="male">
			<description>Output 0</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-O0-pin" terminalId="connector-O0-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-O0-pin" terminalId="connector-O0-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-O0-pad"/>
					<p layer="copper0" svgId="connector-O0-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-I1A" name="I1A" type="male">
			<description>Input 1A</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-I1A-pin" terminalId="connector-I1A-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-I1A-pin" terminalId="connector-I1A-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-I1A-pad"/>
					<p layer="copper0" svgId="connector-I1A-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-I1B" name="I1B" type="male">
			<description>Input 1B</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-I1B-pin" terminalId="connector-I1B-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-I1B-pin" terminalId="connector-I1B-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-I1B-pad"/>
					<p layer="copper0" svgId="connector-I1B-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-O1" name="O1" type="male">
			<description>Output 1</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-O1-pin" terminalId="connector-O1-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-O1-pin" terminalId="connector-O1-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-O1-pad"/>
					<p layer="copper0" svgId="connector-O1-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-GND" name="GND" type="male">
			<description>Ground</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-GND-pin" terminalId="connector-GND-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-GND-pin" terminalId="connector-GND-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-GND-pad"/>
					<p layer="copper0" svgId="connector-GND-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-O2" name="O2" type="male">
			<description>Output 2</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-O2-pin" terminalId="connector-O2-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-O2-pin" terminalId="connector-O2-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-O2-pad"/>
					<p layer="copper0" svgId="connector-O2-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-I2B" name="I2B" type="male">
			<description>Input 2B</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-I2B-pin" terminalId="connector-I2B-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-I2B-pin" terminalId="connector-I2B-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-I2B-pad"/>
					<p layer="copper0" svgId="connector-I2B-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-I2A" name="I2A" type="male">
			<description>Input 2A</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-I2A-pin" terminalId="connector-I2A-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-I2A-pin" terminalId="connector-I2A-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-I2A-pad"/>
					<p layer="copper0" svgId="connector-I2A-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-O3" name="O3" type="male">
			<description>Output 3</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-O3-pin" terminalId="connector-O3-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-O3-pin" terminalId="connector-O3-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-O3-pad"/>
					<p layer="copper0" svgId="connector-O3-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-I3B" name="I3B" type="male">
			<description>Input 3B</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-I3B-pin" terminalId="connector-I3B-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-I3B-pin" terminalId="connector-I3B-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-I3B-pad"/>
					<p layer="copper0" svgId="connector-I3B-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-I3A" name="I3A" type="male">
			<description>Input 3A</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-I3A-pin" terminalId="connector-I3A-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-I3A-pin" terminalId="connector-I3A-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-I3A-pad"/>
					<p layer="copper0" svgId="connector-I3A-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-VCC" name="VCC" type="male">
			<description>Positive Power Supply (2.5V ~ 6.0V)</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-VCC-pin" terminalId="connector-VCC-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-VCC-pin" terminalId="connector-VCC-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-VCC-pad"/>
					<p layer="copper0" svgId="connector-VCC-pad"/>
				</pcbView>
			</views>
		</connector>
	</connectors>
</module>