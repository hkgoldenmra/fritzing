<?xml version="1.0" encoding="UTF-8"?>
<module fritzingVersion="0.9.3b" moduleId="e9f8562e-dff0-c089-d5a1-edc17eb9fa28">
	<version>4</version>
	<author>HKGoldenMr.A</author>
	<title>ATtiny85</title>
	<description>ATtiny (also known as TinyAVR) are a subfamily of the popular 8-bit AVR microcontrollers, which typically has fewer features, fewer I/O pins, and less memory than other AVR series chips. The first members of this family were released in 1999 by Atmel (later acquired by Microchip Technology in 2016).</description>
	<date>2020-07-12</date>
	<label>IC</label>
	<tags>
		<tag>IC</tag>
		<tag>DIP</tag>
	</tags>
	<properties>
		<property name="family">A IC</property>
		<property name="package">DIP</property>
		<property name="pins">8</property>
		<property name="type">ATtiny85</property>
	</properties>
	<views>
		<iconView>
			<layers image="icon/ic-dip-ATtiny85.svg">
				<layer layerId="icon"/>
			</layers>
		</iconView>
		<breadboardView>
			<layers image="breadboard/ic-dip-ATtiny85.svg">
				<layer layerId="breadboard"/>
			</layers>
		</breadboardView>
		<schematicView>
			<layers image="schematic/ic-dip-ATtiny85.svg">
				<layer layerId="schematic"/>
			</layers>
		</schematicView>
		<pcbView>
			<layers image="pcb/ic-dip-ATtiny85.svg">
				<layer layerId="copper1"/>
				<layer layerId="silkscreen"/>
				<layer layerId="copper0"/>
			</layers>
		</pcbView>
	</views>
	<connectors>
		<connector id="connector-RST" name="RST" type="male">
			<description>Reset</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-RST-pin" terminalId="connector-RST-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-RST-pin" terminalId="connector-RST-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-RST-pad"/>
					<p layer="copper0" svgId="connector-RST-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-D3/A2" name="D3/A2" type="male">
			<description>Digital 3 / Analog 2</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-D3/A2-pin" terminalId="connector-D3/A2-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-D3/A2-pin" terminalId="connector-D3/A2-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-D3/A2-pad"/>
					<p layer="copper0" svgId="connector-D3/A2-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-D4/A1" name="D4/A1" type="male">
			<description>Digital 4 / Analog 1</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-D4/A1-pin" terminalId="connector-D4/A1-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-D4/A1-pin" terminalId="connector-D4/A1-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-D4/A1-pad"/>
					<p layer="copper0" svgId="connector-D4/A1-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-GND" name="GND" type="male">
			<description>Ground</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-GND-pin" terminalId="connector-GND-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-GND-pin" terminalId="connector-GND-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-GND-pad"/>
					<p layer="copper0" svgId="connector-GND-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-D0~" name="D0~" type="male">
			<description>Digital 0, PWM, MOSI</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-D0~-pin" terminalId="connector-D0~-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-D0~-pin" terminalId="connector-D0~-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-D0~-pad"/>
					<p layer="copper0" svgId="connector-D0~-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-D1~" name="D1~" type="male">
			<description>Digital 1, PWM, MISO</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-D1~-pin" terminalId="connector-D1~-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-D1~-pin" terminalId="connector-D1~-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-D1~-pad"/>
					<p layer="copper0" svgId="connector-D1~-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-D2/A0" name="D2/A0" type="male">
			<description>Digital 2 / Analog 0, SCK</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-D2/A0-pin" terminalId="connector-D2/A0-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-D2/A0-pin" terminalId="connector-D2/A0-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-D2/A0-pad"/>
					<p layer="copper0" svgId="connector-D2/A0-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-VCC" name="VCC" type="male">
			<description>Positive Supply Voltage (2.7V ~ 5.5V)</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-VCC-pin" terminalId="connector-VCC-terminal"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-VCC-pin" terminalId="connector-VCC-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-VCC-pad"/>
					<p layer="copper0" svgId="connector-VCC-pad"/>
				</pcbView>
			</views>
		</connector>
	</connectors>
</module>