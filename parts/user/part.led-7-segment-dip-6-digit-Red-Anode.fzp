<?xml version="1.0" encoding="UTF-8"?>
<module fritzingVersion="0.9.3b" moduleId="c94a04a9-ff65-8459-a79a-2355b8699444">
	<version>4</version>
	<author>HKGoldenMr.A</author>
	<title>LED 7 Segment DIP 6 Digit Red Anode</title>
	<date>2020-07-07</date>
	<label>7SEG</label>
	<tags>
		<tag>LED</tag>
		<tag>7SEG</tag>
		<tag>DIP</tag>
	</tags>
	<properties>
		<property name="color">Red</property>
		<property name="digits">6</property>
		<property name="common">Anode</property>
		<property name="family">A LED 7 Segment</property>
		<property name="package">DIP</property>
	</properties>
	<views>
		<iconView>
			<layers image="icon/led-7-segment-dip-6-digit-Red-Anode.svg">
				<layer layerId="icon"/>
			</layers>
		</iconView>
		<breadboardView>
			<layers image="breadboard/led-7-segment-dip-6-digit-Red-Anode.svg">
				<layer layerId="breadboard"/>
			</layers>
		</breadboardView>
		<schematicView>
			<layers image="schematic/led-7-segment-dip-6-digit-Red-Anode.svg">
				<layer layerId="schematic"/>
			</layers>
		</schematicView>
		<pcbView>
			<layers image="pcb/led-7-segment-dip-6-digit-Red-Anode.svg">
				<layer layerId="copper1"/>
				<layer layerId="silkscreen"/>
				<layer layerId="copper0"/>
			</layers>
		</pcbView>
	</views>
	<connectors>
		<connector id="connector-E" name="E Cathode" type="male">
			<description>E Cathode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-E-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-E-pin" terminalId="connector-E-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-E-pad"/>
					<p layer="copper0" svgId="connector-E-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-D" name="D Cathode" type="male">
			<description>D Cathode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-D-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-D-pin" terminalId="connector-D-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-D-pad"/>
					<p layer="copper0" svgId="connector-D-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-P" name="P Cathode" type="male">
			<description>P Cathode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-P-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-P-pin" terminalId="connector-P-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-P-pad"/>
					<p layer="copper0" svgId="connector-P-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-C" name="C Cathode" type="male">
			<description>C Cathode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-C-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-C-pin" terminalId="connector-C-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-C-pad"/>
					<p layer="copper0" svgId="connector-C-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-G" name="G Cathode" type="male">
			<description>G Cathode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-G-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-G-pin" terminalId="connector-G-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-G-pad"/>
					<p layer="copper0" svgId="connector-G-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-1" name="1 Anode" type="male">
			<description>1 Anode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-1-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-1-pin" terminalId="connector-1-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-1-pad"/>
					<p layer="copper0" svgId="connector-1-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-2" name="2 Anode" type="male">
			<description>2 Anode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-2-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-2-pin" terminalId="connector-2-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-2-pad"/>
					<p layer="copper0" svgId="connector-2-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-3" name="3 Anode" type="male">
			<description>3 Anode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-3-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-3-pin" terminalId="connector-3-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-3-pad"/>
					<p layer="copper0" svgId="connector-3-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-A" name="A Cathode" type="male">
			<description>A Cathode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-A-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-A-pin" terminalId="connector-A-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-A-pad"/>
					<p layer="copper0" svgId="connector-A-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-F" name="F Cathode" type="male">
			<description>F Cathode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-F-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-F-pin" terminalId="connector-F-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-F-pad"/>
					<p layer="copper0" svgId="connector-F-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-4" name="4 Anode" type="male">
			<description>4 Anode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-4-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-4-pin" terminalId="connector-4-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-4-pad"/>
					<p layer="copper0" svgId="connector-4-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-5" name="5 Anode" type="male">
			<description>5 Anode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-5-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-5-pin" terminalId="connector-5-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-5-pad"/>
					<p layer="copper0" svgId="connector-5-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-B" name="B Cathode" type="male">
			<description>B Cathode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-B-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-B-pin" terminalId="connector-B-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-B-pad"/>
					<p layer="copper0" svgId="connector-B-pad"/>
				</pcbView>
			</views>
		</connector>
		<connector id="connector-6" name="6 Anode" type="male">
			<description>6 Anode</description>
			<views>
				<breadboardView>
					<p layer="breadboard" svgId="connector-6-pin"/>
				</breadboardView>
				<schematicView>
					<p layer="schematic" svgId="connector-6-pin" terminalId="connector-6-terminal"/>
				</schematicView>
				<pcbView>
					<p layer="copper1" svgId="connector-6-pad"/>
					<p layer="copper0" svgId="connector-6-pad"/>
				</pcbView>
			</views>
		</connector>
	</connectors>
</module>